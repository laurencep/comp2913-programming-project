import os
import sys
import unittest
from app import app


# blocking prints from functions in routes.py
def block_print():
    sys.stdout = open(os.devnull, 'w')


# restore prints
def unblock_print():
    sys.stdout = sys.__stdout__


class RouteTests(unittest.TestCase):
    """
        Class for testing response code of routes
    """

    # set up function for tests
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False

        # disable login to check if websites requiring login can load
        app.config['LOGIN_DISABLED'] = True

        block_print()
        self.app = app.test_client()

    # tear down function for tests
    def tearDown(self):
        unblock_print()
        pass

    # testing routes for correct response code
    # asserting response code against 200 (HTTP OK response code)
    def test_index_route(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_login_route(self):
        response = self.app.get('/login', follow_redirects=False)
        self.assertEqual(response.status_code, 200)
    
    def test_logout_route(self):
        response = self.app.get('/logout', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_login_employee_route(self):
        response = self.app.get('/login_employee', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_register_route(self):
        response = self.app.get('/register', follow_redirects=False)
        self.assertEqual(response.status_code, 200)

    def test_manager_route(self):
        response = self.app.get('/manager', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
    
    def test_configure_facilities_route(self):
        response = self.app.get('/configure_facilities', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_register_employee_route(self):
        response = self.app.get('/register_employee', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
    
    def test_payment_route(self):
        response = self.app.get('/payment', follow_redirects=False)
        self.assertEqual(response.status_code, 200)
    
    def test_cancel_membership_route(self):
        response = self.app.get('/cancel_membership', follow_redirects=True)
        self.assertEqual(response.status_code, 200)