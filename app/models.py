ACCESS = {
    'client': 0,
    'employee': 1,
    'manager': 2
}


class User(object):
    def __init__(self, email, access):
        self.email = email
        self.access = access

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return [self.email, self.access]

    # methods used in roles and decorators

    def allowed(self, access_level):
        return self.access >= access_level

    @property
    def is_client(self):
        return self.access == ACCESS['client']

    @property
    def is_employee(self):
        return self.access == ACCESS['employee']

    @property
    def is_manager(self):
        return self.access == ACCESS['manager']



