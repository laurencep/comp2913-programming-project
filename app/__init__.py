from flask import Flask
from flask_mail import Mail, Message
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import os

# create application object
app = Flask(__name__)
if 'FIREBASE_KEY' in os.environ:
    cred = firebase_admin.credentials.Certificate(eval(os.environ.get('FIREBASE_KEY')))
else:
    cred = firebase_admin.credentials.Certificate('instance/firebase.json')
firebase_admin.initialize_app(cred)
db = firestore.client()

# configuring mail settings
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
app.config['MAIL_USERNAME'] = 'groupfivegym@gmail.com'
app.config['MAIL_PASSWORD'] = 'L[9p8}p>,{!k)ZK'
app.config['MAIL_DEFAULT_SENDER'] = ('GroupFive Gym Team', 'groupfivegym@gmail.com')
app.config['MAIL_ASCII_ATTACHMENTS'] = False


# initialising mail
mail = Mail(app)

from app import routes
