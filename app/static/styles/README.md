**bootstrap.min.css** and **timetablejs.css** are copyright of their respective
owners and used under the MIT and GNU General Public Licenses respectively. Any
other files in this directory are original source code written by us.