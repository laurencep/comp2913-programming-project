
from datetime import date, time, datetime
from fpdf import FPDF
# generate a receipt


def generate_receipt(payment_date, product_type, order, email, card_num, amount, user):
    
    date = payment_date.strftime("%d/%m/%Y")
    time = payment_date.strftime("%H:%M:%S")
    
    pdf = FPDF(orientation='P', unit='pt', format='A5')
    pdf.add_page()
    pdf.set_font("Times", "B", 24)
    pdf.cell(0, 80, "Receipt From Gym", 0, 1, "C")

    pdf.set_font("Times", "B", 14)
    pdf.cell(100, 25, "Payment Date:")

    pdf.set_font("Times", "", 12)
    pdf.cell(0, 25, "{}  {}".format(time, date))
    pdf.cell(0, 15, "", 0, 1)
    
    pdf.set_font("Times", "B", 14)
    pdf.cell(100, 25, "Payee's email: :")

    pdf.set_font("Times", "", 12)
    pdf.cell(0, 25, "{}".format(email))
    pdf.cell(0, 15, "", 0, 1)
    
    pdf.set_font("Times", "B", 14)
    pdf.cell(100, 25, "Card Number: ")
    
    pdf.set_font("Times", "", 12)
    pdf.cell(0, 25, "{}".format(card_num))
    pdf.cell(0, 30, "", 0, 1)

    pdf.set_font("Times", "B", 14)
    pdf.cell(100, 25, "Order Type: ")

    pdf.set_font("Times", "", 12)
    pdf.cell(0, 25, "{}".format(product_type))
    pdf.cell(0, 15, "", 0, 1)

    pdf.set_font("Times", "B", 14)
    pdf.cell(100, 25, "Order: ")

    pdf.set_font("Times", "", 12)
    pdf.cell(0, 25, "{}".format(order))
    pdf.cell(0, 25, "", 0, 1)

    pdf.cell(0, 0, "", 1, 1)

    pdf.set_font("Times", "B", 14)
    pdf.cell(100, 25, "Payment Total:")

    pdf.set_font("Times", "", 12)
    pdf.cell(0, 25, "£{}".format(amount), 0, 1)

    name = "app/tempReceipts/" + user + ".pdf"

    return pdf.output(name, 'F')



