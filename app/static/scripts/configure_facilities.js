// This script provides the functionality for the configure facilities page. It
// collects the current state of the facilities collection and allows the
// manager to change/add/remove facilities and activities.

// Get facilities from meta tag and format into json object
var facilities_ = $("#facilities").data("facilities");
var facilities = {};
facilities_.forEach(function(facility){
    facilities[facility.name] = facility;
});
Object.entries(facilities).forEach(function(facility){
    activitiesObject = {};
    facility[1]["activities"].forEach(function(activity){
        activitiesObject[activity.name] = activity;
    });
    facility[1]["activities"] = activitiesObject;
});

// Get reference to ul to store facilty list
var facilityList = $("#facility-list");

// Create list entry for each facility
Object.entries(facilities).forEach(function(facility){
    var facilityListEntry = document.createElement("a");
    facilityListEntry.classList.add("list-group-item", "list-group-item-action");
    facilityListEntry.innerHTML = facility[0];
    
    // When entry is clicked, expand
    facilityListEntry.addEventListener("click", function(){

        // Display a list of editable facility properties
        var facilityPropertyList = document.createElement("ul");
        facilityPropertyList.classList.add("list-group");
        Object.entries(facility[1]).forEach(function(property){
            if (property[0] != "activities"){
                facilityPropertyList.append(createPropertyListEntry(property[0], typeof(property[1]), property[1], facilities[facility[0]]));
            }
        });

        // Display activities button
        var activityListButton = document.createElement("a");
        activityListButton.classList.add("list-group-item", "list-group-item-action");
        activityListButton.innerHTML = "Activities";

        // Expand when clicked
        activityListButton.addEventListener("click", function(){

            // Display a list of activities
            var activityList = document.createElement("ul");
            activityList.classList.add("list-group");
            Object.entries(facility[1]["activities"]).forEach(function(activity){
                var activityListEntry = document.createElement("a");
                activityListEntry.classList.add("list-group-item", "list-group-item-action");
                activityListEntry.innerHTML = activity[0];

                // When entry is clicked, expand
                activityListEntry.addEventListener("click", function(){

                    // Display a list of editable activity properties
                    var activityPropertyList = document.createElement("ul");
                    activityPropertyList.classList.add("list-group");
                    Object.entries(activity[1]).forEach(function(property){
                        activityPropertyList.append(createPropertyListEntry(property[0], typeof(property[1]), property[1], facilities[facility[0]]["activities"][activity[0]]));
                    });

                    // Add delete activity button
                    activityPropertyList.append(createDeleteButton(facilities[facility[0]]["activities"], activity[0], activityListEntry));

                    activityListEntry.append(activityPropertyList);

                    // Once expanded, remove event listener
                    activityListEntry.removeEventListener("click", arguments.callee);
                    activityListEntry.classList.remove("list-group-item-action");
                });
                activityList.append(activityListEntry);
            });

            // Add option to add activity to facility
            activityList.append(createAddActivityButton(facility[0]))

            activityListButton.append(activityList);

            // Once expanded, remove event listener
            activityListButton.removeEventListener("click", arguments.callee);
            activityListButton.classList.remove("list-group-item-action");
        });
        facilityPropertyList.append(activityListButton);

        // Add delete facility button
        facilityPropertyList.append(createDeleteButton(facilities, facility[0], facilityListEntry));

        this.append(facilityPropertyList);

        // Once expanded, remove event listener
        this.removeEventListener("click", arguments.callee);
        this.classList.remove("list-group-item-action");
    });
    facilityList.append(facilityListEntry);
});

// Create add facility button
var addFacilityListEntry = document.createElement("li");
addFacilityListEntry.classList.add("list-group-item");
var addFacilityButton = document.createElement("button");
addFacilityButton.classList.add("btn", "btn-primary", "btn-block");
addFacilityButton.innerHTML = "Add Facility";

// On click add a new facility to the list
addFacilityButton.addEventListener("click", function(){
    var newFacilityListEntry = document.createElement("li");
    newFacilityListEntry.classList.add("list-group-item");
    newFacilityListEntry.innerHTML = "New Facility";

    // Create unique reference to add to JSON object
    var d = new Date();
    var ftag = d.getTime();
    facilities[ftag] = {};
    facilities[ftag]["activities"] = {};

    // Display a list of editable activity properties
    var newFacilityPropertyList = document.createElement("ul");
    newFacilityPropertyList.classList.add("list-group");

    properties = {"name": "string", "capacity": "number"}

    Object.entries(properties).forEach(function(property){
        newFacilityPropertyList.append(createPropertyListEntry(property[0], property[1], null, facilities[ftag]));
    });
    
    // Display an initially empty list of activities
    var newFacilityPropertyListActivitiesEntry = document.createElement("li");
    newFacilityPropertyListActivitiesEntry.classList.add("list-group-item");
    newFacilityPropertyListActivitiesEntry.innerHTML = "Activities";

    // Add button to add activity
    var newActivitiesList = document.createElement("ul");
    newActivitiesList.classList.add("list-group");
    newActivitiesList.append(createAddActivityButton(ftag))

    newFacilityPropertyListActivitiesEntry.append(newActivitiesList);
    newFacilityPropertyList.append(newFacilityPropertyListActivitiesEntry);

    // Add button to delete facility
    newFacilityPropertyList.append(createDeleteButton(facilities, ftag, newFacilityListEntry));

    newFacilityListEntry.append(newFacilityPropertyList)
    $(addFacilityListEntry).before(newFacilityListEntry);
});
addFacilityListEntry.append(addFacilityButton)
facilityList.append(addFacilityListEntry);

// Creates an entry in a facility/activity property list.
// Parameters:
// - name - The name of the parameter
// - dataType - The data type of the parameter
// - defaultValue - The default value for the property, can be null
// - object - Reference to the properties' parent object, either a facilty or
// activity in the JSON object
function createPropertyListEntry(name, dataType, defaultValue, object){
    var entry = document.createElement("li");
    entry.classList.add("list-group-item");

    // Create a label
    var label = document.createElement("label");
    label.setAttribute("for", name);
    label.innerHTML = name;
    entry.append(label);

    switch (dataType) {
        // For string and number types create a single input box
        case "string":
        case "number":
            var input = document.createElement("input");
            input.classList.add("form-control");
            input.setAttribute("name", name);
            input.setAttribute("type", (dataType == "string") ? "text" : "number");
            
            // Set default value if given
            if (defaultValue != null) {
                input.value = defaultValue;
            }

            // Reflect changes in the JSON object
            input.addEventListener((dataType == "string") ? "keyup" : "change", function(){
                object[name] = (dataType == "string") ? this.value : parseInt(this.value); 
            });

            entry.append(input);
            break;
        case "boolean":
            // For boolean types create radio buttons
            entry.append(document.createElement("br"));
            [true, false].forEach(function(option){
                var label = document.createElement("label");
                label.classList.add("radio-inline", "mr-2");
                label.innerHTML = option;
                
                var input = document.createElement("input");
                input.setAttribute("type", "radio");
                input.setAttribute("name", name);

                // Reflect changes in the JSON object
                input.addEventListener("change", function(){
                    object[name] = option;
                });
        
                // Set default value if given
                if (defaultValue == option) {
                    input.checked = true;
                }

                entry.append(input);
                entry.append(label);
            })
            break;
    }
    return entry;
}

// Creates an add activity button for use in existing and new facilities
// Parameters:
// - faciltyName - The name of the facility to create the activity for
function createAddActivityButton(facilityName){
    var addActivityListEntry = document.createElement("li");
    addActivityListEntry.classList.add("list-group-item");
    
    var addActivityButton = document.createElement("button");
    addActivityButton.classList.add("btn", "btn-primary", "btn-block");
    addActivityButton.innerHTML = "Add Activity";

    addActivityButton.addEventListener("click", function(){
        var newActivityListEntry = document.createElement("li");
        newActivityListEntry.classList.add("list-group-item");
        newActivityListEntry.innerHTML = "New Activity";

        // Create unique reference to add to JSON object
        d = new Date();
        var atag = d.getTime();
        facilities[facilityName]["activities"][atag] = {};

        // Display a list of editable activity properties
        var newActivityPropertyList = document.createElement("ul");
        newActivityPropertyList.classList.add("list-group");

        activityProperties = {"duration": "number", "public": "boolean", "name": "string", "price": "number"}

        Object.entries(activityProperties).forEach(function(property){
            newActivityPropertyList.append(createPropertyListEntry(property[0], property[1], null, facilities[facilityName]["activities"][atag]));
        });

        // Add a delete activity button
        newActivityPropertyList.append(createDeleteButton(facilities[facilityName]["activities"], atag, newActivityListEntry));
        newActivityListEntry.append(newActivityPropertyList);
        $(addActivityListEntry).before(newActivityListEntry);
    });
    addActivityListEntry.append(addActivityButton);
    return addActivityListEntry;
}

// Creates a delete button for a facility or activity
// Parameters:
// - object - The parent of the facility or activity object in the JSON object
// - propertyName - The facility or activity object in the JSON object to delete
// - element - The HTML element representing the facility or activity
function createDeleteButton(parentObject, propertyName, element){
    var activityListEntryDeleteButton = document.createElement("button");
    activityListEntryDeleteButton.classList.add("btn", "btn-danger", "mt-2");
    activityListEntryDeleteButton.innerHTML = "Delete"

    // When button clicked, remove object from JSON object and delete HTML
    // element from list
    activityListEntryDeleteButton.addEventListener("click", function(){
        delete parentObject[propertyName];
        $(element).remove();
    });
    return activityListEntryDeleteButton;
}

// When update button is clicked send updated facilities object to server
$("#update").click(function(){
    $(this).prop("disabled", true);
    $(this).html("Updating...")
    $.ajax({
        url: "/configure_facilities_update",
        type: "GET",
        data: {d: JSON.stringify(facilities)},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response){
            // Reload page on success to reflect changes
            location.reload();
        },
        error: function(error){
            console.log(error);
        }
    });
});