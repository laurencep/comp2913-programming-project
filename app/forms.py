from flask import session
from flask_wtf import FlaskForm
from flask_login import current_user
from wtforms import StringField, PasswordField, IntegerField, SubmitField, SelectField
from wtforms.fields.html5 import DateField
from wtforms import TimeField
from wtforms.validators import (
    DataRequired, Length, Email, EqualTo, NumberRange, Optional, ValidationError
)
from datetime import datetime
from app import db
import datetime


class RegisterForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired(), Length(1, 30)])
    last_name = StringField('Surname', validators=[DataRequired(), Length(1, 30)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired(), Length(1, 30)])
    password_confirm = PasswordField('Confirm Password',
                                     validators=[DataRequired(),
                                                 EqualTo('password',
                                                         message='Passwords do not match')])
    submit = SubmitField('Register')


class RegisterEmployeeForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(1, 40)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired(), Length(1, 30)])
    password_confirm = PasswordField('Confirm Password',
                                     validators=[DataRequired(),
                                                 EqualTo('password',
                                                         message='Passwords do not match')])
    salary = IntegerField('Salary',
                          validators=[DataRequired(),
                                      NumberRange(min=0, max=500,
                                                  message=
                                                  'You must pay your employee below minimum wage')])
    submit = SubmitField('Register')


class LogInForm(FlaskForm):
    email = StringField('Email Address', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign In')


class PaymentForm(FlaskForm):
    card_type = SelectField('Card Type', validators=[DataRequired()], choices=[('visa', 'Visa'), ('mastercard','Mastercard')])
    cardholder_name = StringField('Cardholder Name', validators=[DataRequired(
        message="Cardholder name cannot be blank")])
    card_number = StringField('Card Number', validators=[DataRequired(
        message="Card number cannot be blank"), Length(
        16, 16, message="Card number must be 16 characters")])
    expiry_month = SelectField('Expiry Month', choices=[('01', '01'), ('02', '02'), ('03', '03'),
                                                        ('04', '04'), ('05', '05'), ('06', '06'),
                                                        ('07', '07'), ('08', '08'), ('09', '09'),
                                                        ('10', '10'), ('11', '11'), ('12', '12')],
                               validators=[DataRequired()])
    expiry_year = StringField('Expiry Year', validators=[DataRequired(
        message="Expiry year cannot be blank"), Length(
        2, 2, message="Expiry year must be in form YY")])
    security_code = StringField('Security Code', validators=[DataRequired(
        message="Security code cannot be blank"), Length(
        3, 3, message="Security code must be 3 characters")])
    submit = SubmitField('Confirm Payments')

    # checking if the card has expired
    def validate_expiry_year(self, field):
        # place in if statement to stop unintentional error message appearing
        if len(self.expiry_year.data) == 2:
            present = datetime.date.today()
            card_expiry_year = "20" + self.expiry_year.data
            card_expiry_month = self.expiry_month.data

            # create a datetime on first day of month card expires
            card_date = card_expiry_year + " " + card_expiry_month + " 01"
            card_date = datetime.datetime.strptime(card_date, "%Y %m %d").date()

            # if this date is smaller than present date, card has expired
            if card_date < present:
                raise ValidationError("This card has expired")


class BookingFormStage1(FlaskForm):
    facility_select = SelectField('Select Facility')
    date_select = DateField('Select date of booking')
    submit = SubmitField('submit')

    def __init__(self):
        super(BookingFormStage1, self).__init__()
        docs = db.collection('facilities').get()
        facilities = []
        for doc in docs:
            if current_user.is_client:
                doc_activities = db.collection('facilities').document(doc.get(
                    'name').lower()).collection('activities').get()
                # if a private activity found, then client can book something in this facility
                private_found = False
                for doc_activity in doc_activities:
                    if not doc_activity.get('public'):
                        private_found = True
                        break

                if private_found:
                    facility = [doc.get('name'), doc.get('name')]
                    facility = tuple(facility)
                    facilities.append(facility)

            else:
                facility = [doc.get('name'), doc.get('name')]
                facility = tuple(facility)
                facilities.append(facility)
        self.facility_select.choices = [item for item in facilities]


class BookingFormStage2(FlaskForm):
    activity_select = SelectField('Select Activity')

    submit = SubmitField('Submit')

    def __init__(self):
        super(BookingFormStage2, self).__init__()
        facility = session.pop('facility').lower()
        docs = db.collection('facilities').document(facility).collection('activities').get()
        activities = []
        for doc in docs:
            activity = [doc.get('name'), doc.get('name')]
            activity = tuple(activity)
            if doc.get('public'):
                if not current_user.is_client:
                    activities.append(activity)
            else:
                activities.append(activity)
        self.activity_select.choices = [item for item in activities]


class BookingFormStage3(FlaskForm):
    time_select = SelectField('Choose Time of booking')
    user_email = StringField('Enter the email of the user you are booking for (if required)',
                             validators=[Optional(), Email()])
    submit = SubmitField('Submit')

    def __init__(self):
        super(BookingFormStage3, self).__init__()
        # 2d array containing time slots and availability
        # 1 means available, 0 otherwise
        activity_times = [["9:00", 1], ["10:00", 1], ["11:00", 1], ["12:00", 1], ["13:00", 1],
                          ["14:00", 1], ["15:00", 1], ["16:00", 1], ["17:00", 1]]

        date = datetime.datetime.strptime(session.pop('date'), '%a, %d %b %Y %X %Z')
        facility = session.pop('facility')
        activity = session.pop('activity')
        activity_length = db.collection('facilities').document(facility.lower()).collection(
            'activities').document(activity.lower()).get()

        facility_ref = db.document('facilities/'+facility.lower())
        docs = db.collection('bookings').where('facility', '==', facility_ref).get()
        for doc in docs:
            data = doc.to_dict()
            if datetime.datetime.date(data['start time']) == date.date():
                hours = int(((data['activity'].get()).get('duration')) / 60)
                for i in range(9):
                    if activity_times[i][0] in str(datetime.datetime.time(data['start time'])):
                        for j in range(hours):
                            activity_times[i + j][1] = 0
                        break

        available_times = []
        hours = int(activity_length.get('duration')/60)
        session['activity_length'] = hours
        for i in range(9-(hours-1)):
            available = True
            for j in range(hours):
                if activity_times[i+j][1] == 0:
                    available = False
                    break
            if available:
                available_time = [activity_times[i][0] + ":00", activity_times[i][0]]
                available_time = tuple(available_time)
                available_times.append(available_time)

        self.time_select.choices = [item for item in available_times]


class CancelBookingForm(FlaskForm):
    booking_select = SelectField('Select booking to remove')
    submit_button = SubmitField('Delete Booking')

    def __init__(self):
        super(CancelBookingForm, self).__init__()
        docs = db.collection('bookings').get()
        bookings = []
        for doc in docs:
            data = doc.to_dict()
            facility_name = doc.get('facility').get().get('name')
            activity_name = doc.get('activity').get().get('name')
            booking_day = data['start time'].strftime("%d/%m/%y")
            booking_time = data['start time'].strftime("%H:%M:%S")
            booking_string = str(booking_day + ", " + facility_name + ", " + activity_name + ", "
                                 + booking_time+", " + doc.get('username'))
            if current_user.is_client:
                if doc.get('username') == current_user.email:
                    booking = [doc.id, booking_string]
                    booking = tuple(booking)
                    bookings.append(booking)

            else:
                booking = [doc.id, booking_string]
                booking = tuple(booking)
                bookings.append(booking)

        self.booking_select.choices = [item for item in bookings]


class SelectReceiptForm(FlaskForm):
    receipt_select = SelectField('Select Receipt to view')
    submit = SubmitField('View Receipt')

    def __init__(self):
        super(SelectReceiptForm, self).__init__()
        receipts = []

        if current_user.is_client:
            # get all receipts belonging the booking made by current user
            docs = db.collection('receipts').where('email', '==', current_user.email).get()

        else:
            # get receipts from every transaction if viewing as employee/manager
            docs = db.collection('receipts').get()

        for doc in docs:
            receipt = doc.to_dict()
            receipt_date = receipt['payment date'].strftime("[%d/%m/%y at %H:%M:%S] ")
            receipt_string = receipt['email'] + " | " + receipt_date + receipt['order']
            receipt_info = [doc.id, receipt_string]
            receipt_info = tuple(receipt_info)
            receipts.append(receipt_info)

        self.receipt_select.choices = [item for item in receipts]

class MembershipForm(FlaskForm):
    duration_select = SelectField('Select Membership Duration')
    submit = SubmitField('submit')

    def __init__(self):
        super(MembershipForm, self).__init__()
        duration = [["Monthly Cost:9.99£", "Monthly Cost:9.99£"], ["Annual Cost:120£", "Annual Cost:120£"]]

        self.duration_select.choices = [item for item in duration]

class MembershipCancelForm(FlaskForm):
    submit = SubmitField('Confirm cancellation')

    def __init__(self):
        super(MembershipCancelForm, self).__init__()