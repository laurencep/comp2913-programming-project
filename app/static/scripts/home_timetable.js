// Stores the names of the facilities to be used in the timetable
var locations = []

// On page load get the facilities via an ajax call
$.ajax({
    url: "/timetable_get_facilities",
    type: "GET",
    dataType: "json",
    success: function(response){
        locations = response;
    },
    error: function(error){
        console.log(error);
    }
});

// On page load trigger an timetable_date_change ajax call using the current
// date to retrieve all bookings for the current day to dislpay on the timetable
// by default
$.ajax({
    url: "/timetable_date_change",
    type: "GET",
    data: {d: formatDate(Date.now())},
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function(response){
        createTimetable(response)
    },
    error: function(error){
        console.log(error);
    }
});

// When the value of the date input changes, i.e. the user selects another date
// from the flatpickr calendar, empty the timetable and send an ajax call with
// the user's selected date to retrieve bookings for that day
$("#date").change(function(){
    $(".timetable").empty();
    $(".timetable").append("<h3>Loading...</h3>");
    $.ajax({
        url: "/timetable_date_change",
        type: "GET",
        data: {d: $(this).val()},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response){
            createTimetable(response)
        },
        error: function(error){
            console.log(error);
        }
    });
});


// Creates a timetable given an array of events containing the name, location,
// start time and end time
function createTimetable(events) {
    if (events.length > 0) {
        var timetable = new Timetable();
        timetable.setScope(7,22);
        timetable.addLocations(locations);
        events.forEach(function(event){
            timetable.addEvent(
                event[0],
                event[1],
                new Date(parseInt(event[2])),
                new Date(parseInt(event[3]))
            );
        });
        var renderer = new Timetable.Renderer(timetable);
        $(".timetable").empty();
        renderer.draw(".timetable")
    } else {
        $(".timetable").empty();
        $(".timetable").append("<h3>No bookings for selected date</h3>");
    }
}


// A helper function to format the date correctly for timetablejs
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}