import io

from app import app, models, db, receiptGenerator, mail
from app.models import ACCESS
from flask import render_template, flash, redirect, url_for, session, request, send_file
from flask_login import (
    LoginManager,
    current_user,
    login_required,
    login_user,
    logout_user
)
from flask_mail import Mail, Message
import google.cloud.exceptions
import google.api_core.datetime_helpers as datetime_helpers
from app.receiptGenerator import generate_receipt
from .forms import (
    RegisterForm,
    RegisterEmployeeForm,
    LogInForm,
    PaymentForm,
    BookingFormStage1,
    BookingFormStage2,
    BookingFormStage3,
    CancelBookingForm,
    SelectReceiptForm,
    MembershipForm,
	MembershipCancelForm
)
import tempfile
import stat
import hashlib
import os
import datetime
import pytz
import json
import copy
from functools import wraps
import datetime
from datetime import timedelta, datetime

SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


@login_manager.user_loader
def load_user(user_id):
    if user_id[1] == 0:
        doc = db.collection('users').document(user_id[0]).get()
    else:
        doc = db.collection('employees').document(user_id[0]).get()
    if doc.exists:
        return models.User(doc.get('email'), user_id[1])
    else:
        return None


def requires_access_level(access_level):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if not current_user.is_authenticated:
                return redirect(url_for('index'))
            elif not current_user.allowed(access_level):
                flash('Sorry, You dont have access to this page')
                return redirect(url_for('home'))
            return f(*args, **kwargs)
        return decorated_function
    return decorator


@app.route('/')
def index():
    """ implements index url

    If a user is logged in then send them to the home page, if not then send
    them to the login page
    """
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    return redirect(url_for('login'))


@app.route('/home', methods=['GET', 'POST'])
@login_required
def home():
    """ implements home url

    renders home.html template and passes title info to page
    """
    reference = db.collection('bookings').where('username', '==', current_user.email).get()
    user_bookings = list(doc.to_dict() for doc in reference)
    current_doc = db.collection('users').document(current_user.email).get()
    membership_status = current_doc.get('membership')

    return render_template('home.html', title='Home', user_bookings=user_bookings, membership_status=membership_status)

@app.route('/timetable_date_change')
def timetable_date_change():
    """
    This route handles ajax calls sent when the timetable is being used on the
    home page. Returns an array of each event happening on a day sent in the
    request including the events name, location, start time and end time.
    """
    date_start = datetime.strptime(request.args.get('d'), '%Y-%m-%d')
    date_end = date_start + timedelta(days=1)
    bookings = db.collection('bookings').where('`start time`', '>', date_start).where('`start time`', '<', date_end).stream()
    bookingsList = []
    for booking in bookings:
        bookingDict = booking.to_dict()
        bookingsList.append([bookingDict.get('activity').get().get('name'), bookingDict.get('facility').get().get('name'), datetime_helpers.to_milliseconds(bookingDict.get('start time')), datetime_helpers.to_milliseconds(bookingDict.get('end time'))])
    return json.dumps(bookingsList)

@app.route('/timetable_get_facilities')
def timetable_get_facilities():
    """
    This route handles ajax calls sent when the timetable is being used on the
    home page
    """
    facilities = db.collection('facilities').stream()
    facilitiesList = []
    for facility in facilities:
        facilitiesList.append(facility.get('name'))
    return json.dumps(facilitiesList)

@app.route('/login', methods=['GET', 'POST'])
def login():
    """
        Implements the login route, providing the login form on a GET request and
        attempting to log in the user on a POST request
    """
    # if user is logged in, send to home page
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    # if form is valid on submit, start login process
    log_in_form = LogInForm()
    if log_in_form.validate_on_submit():
        doc = db.collection('users').document(log_in_form.email.data).get()
        # if email isn't registered, report error to user
        if doc.exists:
            hashed_password = hashlib.md5(log_in_form.password.data.encode('utf-16be')).hexdigest()
            # checks if password matches password hash
            if hashed_password == doc.get('password'):
                user = models.User(doc.get('email'), ACCESS['client'])
                login_user(user)
                return redirect(url_for('home'))
            else:
                flash('Incorrect password')
        else:
            flash('No account with that email')
    return render_template('login.html', title='Login', log_in_form=log_in_form)


@app.route('/login_employee', methods=['GET', 'POST'])
def login_employee():
    """
        Implements the employee login route, providing the login form on a GET
        request and attempting to log in the employee on a POST request
    """
    # if employee is logged in, send to employee page
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    # if form is valid on submit, start login process
    log_in_form = LogInForm()
    if log_in_form.validate_on_submit():
        doc = db.collection('employees').document(log_in_form.email.data).get()
        # if email isn't registered, report error to user
        if doc.exists:
            hashed_password = hashlib.md5(log_in_form.password.data.encode('utf-16be')).hexdigest()
            # checks if password matches password hash
            if hashed_password == doc.get('password'):
                if doc.get('manager'):
                    user = models.User(doc.get('email'), ACCESS['manager'])
                else:  
                    user = models.User(doc.get('email'), ACCESS['employee'])
                login_user(user)
                return redirect(url_for('home'))
            else:
                flash('Incorrect password')
        else:
            flash('No account with that email')
    return render_template('login_employee.html', title='Login', log_in_form=log_in_form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    """
        Implements the register route, providing the register form on a GET
        request and posting the new user to the database on a POST request
    """
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    register_form = RegisterForm()
    if register_form.validate_on_submit():
        doc = db.collection('users').document(register_form.email.data).get()
        if doc.exists:
            flash('Account with that email already exists')
        else:
            hashed_password = hashlib.md5(
                register_form.password.data.encode('utf-16be')).hexdigest()
            user = {
                'email': register_form.email.data,
                'password': hashed_password,
                'membership': False,
                'first name': register_form.first_name.data,
                'last name': register_form.last_name.data
            }
            db.collection('users').document(register_form.email.data).set(user)
            return redirect(url_for('login'))
    return render_template('register.html', title='Register', register_form=register_form)


@app.route('/manager', methods=['GET', 'POST'])
@requires_access_level(ACCESS['manager'])
def manager():
    """
            Implements the manager route, renders usage statistics from database
            for manager to see in template
        """
    employees = db.collection('employees').stream()
    employees_list = []
    for employee in employees:
        employees_list.append(employee.to_dict())

    users = db.collection('users').stream()
    users_list = []
    for user in users:
        users_list.append(user.to_dict())

    overall_income = 0
    overall_bookings = 0
    facility_overall_income = {}
    facility_overall_bookings = {}
    week_income = 0
    week_bookings = 0
    facility_week_income = {}
    facility_week_bookings = {}
    
    # Get details of all facilities from firebase and put into dictionary
    facilities = {}
    facilities_from_db = db.collection('facilities').stream()
    for facility in facilities_from_db:
        facilities[facility.id] = facility.to_dict()
        facilities[facility.id]['activities'] = {}
        activities = db.collection('facilities').document(facility.id).collection('activities').stream()
        for activity in activities:
            facilities[facility.id]['activities'][activity.id] = activity.to_dict()
        facility_overall_income[facility.id] = 0
        facility_overall_bookings[facility.id] = 0
        facility_week_income[facility.id] = 0
        facility_week_bookings[facility.id] = 0

    # Calculate start and end of week
    today = pytz.utc.localize(datetime.today())
    today = today.replace(hour=00, minute=00, second=00)
    beg_week = today - timedelta(days=today.weekday())
    end_week = beg_week + timedelta(days=6)
    end_week = end_week.replace(hour=23, minute=59, second=59)

    # Loop through bookings, generating overall and weekly incomes and numbers
    # of bookings for each facility.
    bookings = db.collection('bookings').stream()
    for booking in bookings:
        overall_bookings = overall_bookings + 1
        facility_id = booking.get('facility').id
        activity_price = booking.get('activity').get().get('price')
        overall_income = overall_income + activity_price
        facility_overall_bookings[facility_id] = facility_overall_bookings[facility_id] + 1
        facility_overall_income[facility_id] = facility_overall_income[facility_id] + activity_price
        start_time = booking.get('`start time`')
        if start_time > beg_week and start_time < end_week:
            week_income = week_income + activity_price
            week_bookings = week_bookings + 1
            facility_week_bookings[facility_id] = facility_week_bookings[facility_id] + 1
            facility_week_income[facility_id] = facility_week_income[facility_id] + activity_price

    # Pass information to template
    return render_template('manager.html', title='Manager',
                            employees_list=employees_list,
                            users_list=users_list,
                            facilities=facilities,
                            overall_income=overall_income,
                            overall_bookings=overall_bookings,
                            facility_overall_income=facility_overall_income,
                            facility_overall_bookings=facility_overall_bookings,
                            week_income=week_income,
                            week_bookings=week_bookings,
                            facility_week_income=facility_week_income,
                            facility_week_bookings=facility_week_bookings,
                            facility_names=[facilities[facility]['name'] for facility in facilities.keys()])

@app.route('/configure_facilities', methods=['GET'])
@requires_access_level(ACCESS['manager'])
def configure_facilities():
    """
    Implements the route to allow managers to configure the facilities and their
    activities in the gym.
    """
    facility_list = []
    facilties = db.collection('facilities').stream()
    for facility in facilties:
        activities = db.collection('facilities').document(facility.get('name').lower()).collection('activities').stream()
        activities_list = []
        for activity in activities:
            activities_list.append(activity.to_dict())
        facility_dict = facility.to_dict()
        facility_dict['activities'] = activities_list
        facility_list.append(facility_dict)
    return render_template('configure_facilities.html', title='Configure Facilities', facilities = json.dumps(facility_list))

@app.route('/configure_facilities_update', methods=['GET'])
def configure_facilities_update():
    """
    This is the ajax route for updating the facilities
    """
    facilities = json.loads(request.args.get('d'))
    # Delete any facilities no longer needed
    facilities_from_db = db.collection('facilities').stream()
    for facility in facilities_from_db:
        if facility.get('name') not in facilities:
            activities = facility.reference.collection('activities').stream()
            for activity in activities:
                activity.reference.delete()
            facility.reference.delete()
    # Loop through facilities dictionary from client
    while len(facilities) != 0:
        facility = next(iter(facilities.items()))
        facility_without_activities = copy.deepcopy(facility)
        facility_without_activities[1].pop('activities')
        # Check if facility already exists in database
        facility_from_db = db.collection('facilities').document(facility[0].lower()).get()
        if facility_from_db.exists:
            # Check if name has been changed
            if facility[0] == facility[1]['name']:
                # No name change, just update document
                db.collection('facilities').document(facility[0].lower()).update(facility_without_activities[1])
            else:
                # Name change, get activities in old document, copy to new and delete from old
                activities = db.collection('facilities').document(facility[0].lower()).collection('activities').stream()
                for activity in activities:
                    db.collection('facilities').document(facility[1]['name'].lower()).collection('activities').document(activity.get('name').lower()).set(activity.to_dict())
                    activity.reference.delete()
                # Delete old document, create new
                db.collection('facilities').document(facility[0].lower()).delete()
                db.collection('facilities').document(facility[1]['name'].lower()).set(facility_without_activities[1])
            # Update activities
            # Delete any activities no longer needed
            activities_from_db = db.collection('facilities').document(facility[1]['name'].lower()).collection('activities').stream()
            for activity in activities_from_db:
                if activity.get('name') not in facility[1]['activities']:
                    activity.reference.delete()
            # Loop through remaining activities
            while len(facility[1]['activities']) != 0:
                activity = next(iter(facility[1]['activities'].items()))
                # Check if activity exists in database
                activity_from_db = db.collection('facilities').document(facility[1]['name'].lower()).collection('activities').document(activity[0].lower()).get()
                if activity_from_db.exists:
                    # No name change, just update document
                    if activity[0] == activity[1]['name']:
                        db.collection('facilities').document(facility[1]['name'].lower()).collection('activities').document(activity[0].lower()).update(activity[1])
                    else:
                        # Name change, delete old document and create new
                        db.collection('facilities').document(facility[1]['name'].lower()).collection('activities').document(activity[0].lower()).delete()
                        db.collection('facilities').document(facility[1]['name'].lower()).collection('activities').document(activity[1]['name'].lower()).set(activity[1])
                else:
                    # New activity, add to database
                    db.collection('facilities').document(facility[1]['name'].lower()).collection('activities').document(activity[1]['name'].lower()).set(activity[1])
                del facility[1]['activities'][activity[0]]
        else:
            # New facility, add to database
            db.collection('facilities').document(facility[1]['name'].lower()).set(facility_without_activities[1])
            # Add all activities to database
            for activity in facility[1]['activities'].items():
                db.collection('facilities').document(facility[1]['name'].lower()).collection('activities').document(activity[1]['name'].lower()).set(activity[1])
        del facilities[facility[0]]
    return json.dumps("")


@app.route('/register_employee', methods=['GET', 'POST'])
@requires_access_level(ACCESS['manager'])
def register_employee():
    """
                Implements the register employee route, providing the employee register form on a GET
                request and posting the new employee to the database on a POST request
                Also renders usage statistics from database for manager to see in template
            """
    register_form = RegisterEmployeeForm()
    if register_form.validate_on_submit():
        doc = db.collection('employees').document(register_form.email.data).get()
        if doc.exists:
            flash('Employee account with that email already exists')
        else:
            hashed_password = hashlib.md5(
                register_form.password.data.encode('utf-16be')).hexdigest()
            employee = {
                'date hired': datetime.now(),
                'email': register_form.email.data,
                'manager': False,
                'name': register_form.name.data,
                'password': hashed_password,
                'salary': register_form.salary.data
            }
        db.collection('employees').document(register_form.email.data).set(employee)
        return redirect(url_for('manager'))
    return render_template('register_employee.html', title='Manager',
                           register_form=register_form)


@app.route('/payment', methods=['GET', 'POST'])
@login_required
def payment():
    """
        Implements the payment route, providing payment form on a GET request
    """
    payment_form = PaymentForm()
    if payment_form.validate_on_submit():
        payment_type = session.pop('payment_type')
        session['payment_type'] = payment_type
        if payment_type == "booking":
            # Gets information from booking stage from session
            booking_data = session.pop('booking_data')
            session['booking_data'] = booking_data
            start_time = booking_data['date']
            # obtains references to "tables" in firebase
            activity_ref = db.document('facilities/' + booking_data['facility'].lower() +
                                       '/activities/' + booking_data['activity'].lower())
            facility_ref = db.document('facilities/' + booking_data['facility'].lower())
            activity_length = int(session.pop('activity_length'))
            session['activity_length'] = str(activity_length)
            # calculates end time of activity
            end_time = start_time + timedelta(hours=activity_length)
            # creates database entry to be pushed
            data = {
                'activity': activity_ref,
                'end time': end_time,
                'facility': facility_ref,
                'start time': start_time,
                'username': current_user.email,
            }

            # gathers data needed for receipt entry in database
            now = datetime.now()

            date = start_time.strftime("%d/%m/%Y")
            time = start_time.strftime("%H:%M:%S")

            order = facility_ref.get().get('name') + " on " + date + " at " + time
            cost = booking_data['cost']
            card_num = "************" + payment_form.card_number.data[12:16]

            receipt_data = {
                'payment date': now,
                'type': payment_type,
                'order': order,
                'card number': card_num,
                'email': current_user.email,
                'cost': cost,
            }


            # sending receipt to user email
            generate_receipt(now, payment_type, order, current_user.email, card_num, cost,
                             current_user.email)
            file_path = "tempReceipts/" + current_user.email + ".pdf"

            # create message to be sent to user
            msg = Message('Your Receipt', recipients=[current_user.email])
            msg.body = 'Here is the receipt for you recent order: ' + order

            # add pdf attachment to email
            with app.open_resource(file_path) as pdf:
                msg.attach('receipt.pdf', 'application/pdf', pdf.read())

            os.remove("app/" + file_path)

            # send email
            mail.send(msg)

            # pushes new booking and receipt into database
            db.collection('bookings').add(data)
            db.collection('receipts').add(receipt_data)

            flash('Booking successfully made, receipt has been sent to your email')
            return redirect(url_for('home'))
        if payment_type == "membership":
            date_data = session.pop('date_data')
            session['date_data'] = date_data
            duration = date_data['duration']
            start_time = datetime.now()
            
            if duration == "Monthly Cost:9.99£":
                end_time = start_time + timedelta(days=31)
            if duration=="Annual Cost:120£":
                end_time = start_time + timedelta(days=365)
			
            user_current = db.collection('users').document(current_user.email).get()
			
            user = {
                'email': user_current.get('email'),
                'password': user_current.get('password'),
                'membership': True,
                'first name': user_current.get('`first name`'),
                'last name': user_current.get('`last name`'),
				'membership start': start_time,
				'membership end': end_time
            }
            db.collection('users').document(current_user.email).set(user)
            flash('You are now a member')
            return redirect(url_for('home'))

    return render_template('payment.html', title='Payment', payment_form=payment_form)


@app.route('/booking_1', methods=['GET', 'POST'])
@login_required
def make_booking_stage_1():
    """
        Implements first stage of booking route, allowing users to input a date and facility
        Posts booking data to stage 2
    """
    form = BookingFormStage1()
    if form.validate_on_submit():
        booking_data = {
            'facility': form.facility_select.data,
            'date': form.date_select.data,
            'activity': None,
            'cost': None
        }
        session['booking_data'] = booking_data
        return redirect(url_for('make_booking_stage_2'))

    return render_template('booking_1.html', title='Make Booking', form=form)


@app.route('/booking_2', methods=['GET', 'POST'])
@login_required
def make_booking_stage_2():
    """
    Implement second stage of booking route, allowing users to input an activity
    Posts booking data to stage 3
    """
    booking_data = session.pop('booking_data')
    session['facility'] = booking_data['facility']
    session['booking_data'] = booking_data
    form = BookingFormStage2()
    if form.validate_on_submit():
        booking_data['activity'] = form.activity_select.data
        activity_data = db.collection('facilities').document(booking_data['facility'].lower()).\
            collection('activities').document(form.activity_select.data.lower()).get()

        booking_data['cost'] = activity_data.get('price')
        session['booking_data'] = booking_data
        return redirect(url_for('make_booking_stage_3'))

    return render_template('booking_2.html', title='Make Booking', form=form,
                           facility=booking_data['facility'],
                           date=str(booking_data['date']).replace(" 00:00:00 GMT", ""))


@app.route('/booking_3', methods=['GET', 'POST'])
@login_required
def make_booking_stage_3():
    """
        Implement third stage of booking route, allowing users to pick a time
    """
    booking_data = session.pop('booking_data')
    session['booking_data'] = booking_data
    session['date'] = booking_data['date']
    session['facility'] = booking_data['facility']
    session['activity'] = booking_data['activity']
    form = BookingFormStage3()

    if form.validate_on_submit():

        strdate = booking_data['date'].replace("00:00:00", str(form.time_select.data))
        date = datetime.strptime(strdate, '%a, %d %b %Y %X %Z')

        booking_data['date'] = date

        # clients should be redirected to payment
        if current_user.is_client:
            session['payment_type'] = "booking"
            session['booking_data'] = booking_data
            return redirect(url_for('payment'))

        else:
            if form.user_email.data != "":
                username = form.user_email.data
            else:
                username = current_user.email

            activity_ref = db.document('facilities/' + booking_data['facility'].lower() +
                                       '/activities/' + booking_data['activity'].lower())
            facility_ref = db.document('facilities/' + booking_data['facility'].lower())
            activity_length = int(session.pop('activity_length'))
            session['activity_length'] = str(activity_length)
            end_time = date + timedelta(hours=activity_length)
            data = {
                'activity': activity_ref,
                'end time': end_time,
                'facility': facility_ref,
                'start time': date,
                'username': username,
            }

            # gathers data needed for receipt entry in database if booked on behalf of client
            if form.user_email.data != "":
                now = datetime.now()

                receipt_date = date.strftime("%d/%m/%Y")
                receipt_time = date.strftime("%H:%M:%S")

                order = facility_ref.get().get('name') + " on " + receipt_date + " at " + \
                    receipt_time
                cost = booking_data['cost']
                card_num = "N/A - Payment made with cash"

                receipt_data = {
                    'payment date': now,
                    'type': "booking",
                    'order': order,
                    'card number': card_num,
                    'email': form.user_email.data,
                    'cost': cost,
                }

                # when booking on behalf of client, receipt will be stored in client document
                # if client is signed up, else stored in the employee document
                client = db.collection('users').document(form.user_email.data).get()
                if client.exists:
                    db.collection('receipts').add(receipt_data)
                else:
                    db.collection('receipts').add(receipt_data)

            db.collection('bookings').add(data)
            flash('Booking successfully made')
            return redirect(url_for('home'))

    return render_template('booking_3.html', title='Make Booking', form=form,
                           facility=booking_data['facility'],
                           date=str(booking_data['date']).replace(" 00:00:00 GMT", ""),
                           activity=booking_data['activity'],
                           cost=booking_data['cost']
                           )


@app.route('/cancel_booking', methods=['GET', 'POST'])
@login_required
def cancel_booking():
    """
        Implements cancel booking page, allowing users to search/delete bookings from the database
    """
    form = CancelBookingForm()
    if form.validate_on_submit():
        doc_name = form.booking_select.data

        db.collection('bookings').document(doc_name).delete()
        flash("Booking cancelled successfully")
        return redirect(url_for('home'))

    return render_template('cancel_booking.html', title='Cancel Booking', form=form)


@app.route('/view_receipts', methods=['GET', 'POST'])
@login_required
def view_receipts():
    """
        Displays list of receipts to user, allowing user to select a specific receipt
    """
    form = SelectReceiptForm()

    if form.validate_on_submit():
        receipt = db.collection('receipts').document(form.receipt_select.data).get()
        receipt = receipt.to_dict()

        # get receipt information then send pdf file to user
        payment_date = receipt['payment date']
        payment_type = receipt['type']
        order = receipt['order']
        email = receipt['email']
        card_num = receipt['card number']
        cost = receipt['cost']

        # generates pdf for receipt, the file is given name of current user to ensure no files
        # overwrite during their lifespan
        generate_receipt(payment_date, payment_type, order, email, card_num, cost,
                         current_user.email)
        file_path = "app/tempReceipts/" + current_user.email + ".pdf"

        # once pdf is made, copy into memory and delete the pdf from server storage
        return_data = io.BytesIO()
        with open(file_path, 'rb') as file:
            return_data.write(file.read())
        return_data.seek(0)
        os.remove(file_path)

        return send_file(return_data, attachment_filename='receipt.pdf')

    return render_template('view_receipt.html', title="View Receipts", form=form)

@app.route('/membership', methods=['GET', 'POST'])
@login_required
def get_membership():
    """
        Implements duration selection for membership. The two options are monthly and annually
    """
	
    form = MembershipForm()
	
    date_data = {'duration': form.duration_select.data}
	
    current_doc = db.collection('users').document(current_user.email).get()
    membership_status = current_doc.get('membership')
    if membership_status:
        flash('You are already a member')
        return redirect(url_for('home'))
	
    if form.validate_on_submit():
	
        if current_user.is_client:
            session['payment_type'] = "membership"
            session['date_data'] = date_data
            return redirect(url_for('payment'))

    return render_template('membership.html', title='Get Membership', form=form, date=date_data['duration'])
	
@app.route('/cancel_membership', methods=['GET', 'POST'])
@login_required
def cancel_membership():
    """
        Implements duration selection for membership. The two options are monthly and annually
    """
	
    form = MembershipCancelForm()
	
    if form.validate_on_submit():
	
        if current_user.is_client:
		
            user_current = db.collection('users').document(current_user.email).get()
		
            user = {
                'email': user_current.get('email'),
                'password': user_current.get('password'),
                'membership': False,
                'first name': user_current.get('`first name`'),
                'last name': user_current.get('`last name`'),
	
            }
			
            db.collection('users').document(current_user.email).set(user)
            flash('You are no longer a member')
            return redirect(url_for('home'))

    return render_template('cancel_membership.html', title='Cancel Membership', form=form)

# LEAVE THIS AT THE BOTTOM OF THE FILE
@app.errorhandler(404)
def page_not_found(e):
    """
        Handles any urls requests that dont exist within the website
    """
    flash('The page you tried to access does not exist')
    return redirect(url_for('index'))
